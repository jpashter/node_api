const express = require('express')
const cors = require('cors')

const app = express()

app.use(express.json())
app.use(express.urlencoded({extended: true}))

const db = require('./app/models')

db.mongoose
    .connect(db.url, {
        useNewUrlParser : true,
        useUnifiedTopology: true
        // useFindAndModify : true
    })

    .then(() => {
        console.log(`Database Connected Successfully.!`)
    }).catch((err)=>{
        console.log(`Cannot Connect to the database.!`, err)
        process.exit()
    })

app.get('/', (req, res)=>{
    res.json({
        message: 'welcome to digimart'
    })
})


require('./app/routes/user.routes')(app)
require('./app/routes/product.routes')(app)

// listenport
const port = 8000
app.listen(port, ()=>{
    console.log(`server is runningn on http://localhost:${port}`)
})