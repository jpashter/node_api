const db = require('../models')

const User = db.user

exports.findAll = (req, res) => {
    User.find()
    .then((result) =>{
        res.send(result)
    }).catch((err)=>{
        res.status(500).send({
            message:err.message || "Some error while retrieving data.!"
        })
    })
}

exports.create = (req, res) => {
    const user = new User({
        kode : req.body.kode,
        nama : req.body.nama,
        alamat : req.body.alamat
    })

    user.save(user)
    .then((result) => {
        res.send(result)
    }).catch((err) => {
        res.status(409).send({
            message: err.message || "Failed to create new data.!"
        })
    })
}

exports.show = (req, res) => {
    const id = req.params.id

    User.findById(id)
    .then((result) => {
        res.send(result)
    }).catch((err) => {
        res.status(404).send({
            message:err.message || "Some Error while show data.!"
        })
    })

}

exports.update = (req, res) =>{
    const id = req.params.id

    User.findByIdAndUpdate(id, req.body)
    .then((result)=>{
        if (!result) {
            res.status(404).send({
                message: "User Not Found"
            })
        }
        res.send({
            message: "User was updated.!"
        })
    }).catch((err)=>{
        res.status(404).send({
            message:err.message || "Some Error while update data.!"
        })
    })
}

exports.delete = (req, res) => {
    const id = req.params.id

    User.findByIdAndRemove(id)
    .then((result)=>{
        if (!result) {
            res.status(404).send({
                message: "User Not Found"
            })
        }
        res.send({
            message: "User was deleted.!"
        })
    }).catch((err)=>{
        res.status(404).send({
            message:err.message || "Some Error while delete data.!"
        })
    })
}