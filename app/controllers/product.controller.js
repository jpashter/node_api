const db = require('../models')

const Product = db.product

const findAll = (req, res) =>{
    Product.find()
    .then((result) =>{
        res.send(result)
    }).catch((err)=>{
        res.status(500).send({
            message:err.message || "Some error while retrieving data.!"
        })
    })
}

const create = (req, res) => {
    const product = new Product({
        kode : req.body.kode,
        product : req.body.product,
        thumbnail : req.body.thumbnail,
        stock : req.body.stock,
        price : req.body.price
    })

    product.save(product)
    .then((result) => {
        res.send({
            message: "Product created successfull.!"
        })
    }).catch((err) => {
        res.status(409).send({
            message: err.message || "Failed to create new data.!"
        })
    })
}

const show = (req, res) => {
    const id = req.params.id

    Product.findById(id)
    .then((result) =>{
        res.send(result)
    }).catch((err)=>{
        res.status(409).send({
            message : err.message || "Some error while get single product.!"
        })
    })
}

const update = (req, res) => {
    const id = req.params.id

    Product.findByIdAndUpdate(id, req.body)
    .then((result) => {
        if(!result){
            res.status(404).send({
                message:"some error or product not found"
            })
        }
        res.send({
            messaage : "Product Was Updated.!"
        })
    }).catch((err) => {
        res.status(409).send({
            message : err.messaage || "some error while update prouct.!"
        })
    })
}

const remove = (req, res) => {
    const id = req.params.id

    Product.findByIdAndRemove(id)
    .then((result)=>{
        if (!result) {
            res.status(404).send({
                message: "Product Not Found"
            })
        }
        res.send({
            message: "Product was deleted.!"
        })
    }).catch((err)=>{
        res.status(404).send({
            message:err.message || "Some Error while delete data.!"
        })
    })
}

module.exports = {findAll, create, show, update, remove}