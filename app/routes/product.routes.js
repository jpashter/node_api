module.exports = (app) => {
    const products = require('../controllers/product.controller')
    const router = require('express').Router()

    router.get('/', products.findAll)
    router.get('/:id', products.show)
    router.post('/', products.create)
    router.put('/:id', products.update)
    router.delete('/:id',products.remove)


    app.use('/api/product',router)
}