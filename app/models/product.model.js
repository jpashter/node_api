module.exports = (mongoose) => {
    const schema = mongoose.Schema(
        {
            kode : String,
            product : String,
            thumbnail : String,
            stock : Number,
            price : Number

        },
        {timestamps : true}
    )

    schema.method("toJSON", function() {
        const {__v, _id, ...object} = this.toObject()
        object.id = _id

        return object
    })

    const Product = mongoose.model("products", schema)

    return Product
}